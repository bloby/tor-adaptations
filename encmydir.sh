#!/bin/bash

sudo apt update
sudo apt install -y cryptsetup ecryptfs-utils rsync
sudo modprobe ecryptfs
#sudo ecryptfs-setup-swap # breaks hibernation
sudo ecryptfs-migrate-home -u <USER>
