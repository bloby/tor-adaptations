#!/bin/bash

[ "$(id -u)" != "0" ] && echo "You must run this script as root user." && exit 1

apt update
apt install -y shorewall shorewall6

interfaces=$(ifconfig | cut -d: -f1 | grep -v '^ ' | grep -v '^$' | grep -v lo)

for shorewall in shorewall shorewall6
do
  # create needed files
  file=zones
  cp /usr/share/$shorewall/configfiles/$file /etc/$shorewall/
  echo "net		ip" >> /etc/$shorewall/$file
  echo "dck		ip" >> /etc/$shorewall/$file

  file=interfaces
  cp /usr/share/$shorewall/configfiles/$file /etc/$shorewall/
  for interface in $interfaces
  do
    echo "net		$interface" >> /etc/$shorewall/$file
    echo "dck		docker0" >> /etc/$shorewall/$file
  done

  file=rules
  cp /usr/share/$shorewall/configfiles/$file /etc/$shorewall/

  file=policy
  cp /usr/share/$shorewall/configfiles/$file /etc/$shorewall/
  echo "fw		net		ACCEPT" >> /etc/$shorewall/$file
  echo "fw		dck		ACCEPT" >> /etc/$shorewall/$file
  echo "net		fw		DROP    warn" >> /etc/$shorewall/$file
  echo "all		all		REJECT  info" >> /etc/$shorewall/$file

  # permit autostart config
  sed -i 's!startup=0!startup=1!' /etc/default/$shorewall

  # start and systemctl enable shorewall
  $shorewall check && \
  $shorewall start && \
  systemctl enable $shorewall && \
  echo "[*] $shorewall ... OK" && \
  continue

  echo "[*] $shorewall ... failed"
done
